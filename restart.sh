#!/bin/bash
MAIN_NAME=shixun-0.0.1-SNAPSHOT
kill $(pgrep -f ${MAIN_NAME}) 2>/dev/null
cd target
JAVA_OPTS="-Xms256m -Xmx256m"
nohup java ${JAVA_OPTS} -jar ${MAIN_NAME}.jar 2>&1 &