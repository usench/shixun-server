drop table if exists sx_group;
CREATE TABLE `sx_group` (
      `id` int primary key auto_increment,
      `group_unique_id` varchar(16) NOT NULL COMMENT '群唯一码',
      `group_name` varchar(32) DEFAULT NULL COMMENT '描述'
);
drop table if exists sx_member;
CREATE TABLE `sx_member` (
     `id` int primary key auto_increment,
     `member_unique_id` varchar(16) NOT NULL COMMENT '成员唯一id',
     `member_pass` varchar(32) NOT NULL COMMENT '密钥',
     `member_nickname` varchar(32) NOT NULL DEFAULT '' COMMENT '昵称'
) ;