package com.easy.lab.shixun.controller.ws;

import com.easy.lab.shixun.api.service.LoginService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.server.HandshakeInterceptor;

import java.util.Map;

/**
 * ws握手拦截器
 * @Author: https://gitee.com/wesleyOne
 * @Date: 12.07 2019
 */
@Slf4j
@Component
public class SimpleHandshakeInterceptor implements HandshakeInterceptor {

    @Autowired
    private LoginService loginService;

    @Override
    public boolean beforeHandshake(ServerHttpRequest serverHttpRequest, ServerHttpResponse serverHttpResponse, WebSocketHandler webSocketHandler, Map<String, Object> map) throws Exception {
        log.debug("WS握手前 beforeHandshake");
        return true;
    }

    @Override
    public void afterHandshake(ServerHttpRequest serverHttpRequest, ServerHttpResponse serverHttpResponse, WebSocketHandler webSocketHandler, Exception e) {
        log.debug("WS握手后 afterHandshake");
    }
}
