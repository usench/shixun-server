package com.easy.lab.shixun.api.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Map;

/**
 * 消息基体
 * @Author: https://gitee.com/wesleyOne
 * @Date: 12.07 2019
 */
@ApiModel
@Data
public class MessageBase {

    /**
     * 群唯一id
     */
    @ApiModelProperty(value = "群唯一编码")
    private String groupUniqueId;

    /**
     * 消息类型
     * @see com.easy.lab.shixun.common.enums.MessageTypeEnums
     */
    @ApiModelProperty(value = "消息类型",notes="参考MessageTypeEnums")
    private String type;

    /**
     * 消息内容
     */
    @ApiModelProperty(value = "消息内容",notes="字典对象形式")
    private Map<String,Object> massageData;
}
