package com.easy.lab.shixun.api.service;

import com.easy.lab.shixun.api.bo.request.GroupRequest;
import com.easy.lab.shixun.api.bo.response.GroupResponse;
import com.easy.lab.shixun.common.base.BaseException;
import org.springframework.web.socket.WebSocketSession;

import java.util.List;

/**
 * 群成员 相关接口
 * @Author: https://gitee.com/wesleyOne
 * @Date: 12.07 2019
 */
public interface GroupMemberService {

    /**
     * 获取群信息
     * @param groupRequest  群参数
     * @return              群列表
     */
    List<GroupResponse> getGroupList(GroupRequest groupRequest);

    /**
     * 获取群信息详情
     * @param groupRequest  群参数
     * @return              群详情
     */
    GroupResponse getGroupInfo(GroupRequest groupRequest);

    /**
     * 关联用户token到群
     * @param token             用户令牌
     * @param groupUniqueId     群唯一id
     */
    void relateTokenWithGroup(String token, String groupUniqueId) throws BaseException;

    /**
     * 关联token和ws会话
     * @param token             用户令牌
     * @param webSocketSession  ws会话
     */
    void relateTokenWithWebSocketSession(String token, WebSocketSession webSocketSession) throws BaseException;

    /**
     * 移除用户token
     * @param token
     */
    void removeTokenWsSession(String token);

    /**
     * 获取群里的所有用户token
     * @param groupUniqueId
     * @return
     */
    List<String> getMembers(String groupUniqueId);

    /**
     * 判断群是否存在
     * @param groupUniqueId     群唯一id
     * @return
     */
    boolean containGroup(String groupUniqueId);

    /**
     * 判断用户token会话是否存在
     * @param token         用户令牌
     * @return
     */
    boolean containMember(String token);
}
