package com.easy.lab.shixun.api.bo.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 成员接口参数
 *
 * @Author: https://gitee.com/wesleyOne
 * @Date: 12.07 2019
 */
@ApiModel
@Data
public class MemberRequest {
    @ApiModelProperty(value = "成员帐号")
    String memberUniqueId;
    @ApiModelProperty(value = "成员昵称")
    String memberName;
}
