package com.easy.lab.shixun.api.bo.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 群接口参数
 * @Author: https://gitee.com/wesleyOne
 * @Date: 12.07 2019
 */
@ApiModel
@Data
public class GroupRequest {
    @ApiModelProperty(value = "群唯一编码")
    String groupUniqueId;
    @ApiModelProperty(value = "群名称")
    String groupName;
}
