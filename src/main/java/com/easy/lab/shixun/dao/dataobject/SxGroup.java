package com.easy.lab.shixun.dao.dataobject;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/**
 * @Author: https://gitee.com/wesleyOne
 * @Date: 12.01 2019
 */
@Data
public class SxGroup {

    @TableId(value = "id",type = IdType.AUTO)
    Long id;

    String groupUniqueId;
    String groupName;
}
