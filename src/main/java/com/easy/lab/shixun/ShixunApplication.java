package com.easy.lab.shixun;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
@MapperScan(basePackages = {"com.easy.lab.shixun.dao.mapper"})
public class ShixunApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShixunApplication.class, args);
	}

}
