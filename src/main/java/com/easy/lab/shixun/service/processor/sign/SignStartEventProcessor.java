package com.easy.lab.shixun.service.processor.sign;

import com.easy.lab.shixun.common.annotation.SxEventProcessor;
import com.easy.lab.shixun.common.enums.MessageTypeEnums;
import com.easy.lab.shixun.api.bo.request.MessageRequest;
import com.easy.lab.shixun.api.bo.response.MessageResponse;
import com.easy.lab.shixun.service.processor.BaseEventProcessor;
import lombok.extern.slf4j.Slf4j;

/**
 * 签到开始处理器
 */
@SxEventProcessor(messageType = MessageTypeEnums.SIGN_START)
@Slf4j
public class SignStartEventProcessor implements BaseEventProcessor {
    @Override
    public MessageResponse process(MessageRequest requestBody) {

        MessageResponse messageResponse = new MessageResponse();
        messageResponse.setType(requestBody.getType());
        messageResponse.setGroupUniqueId(requestBody.getGroupUniqueId());
        messageResponse.setMassageData(requestBody.getMassageData());

        return messageResponse;
    }
}
