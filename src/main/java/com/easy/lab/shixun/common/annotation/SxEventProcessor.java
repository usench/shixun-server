package com.easy.lab.shixun.common.annotation;

import com.easy.lab.shixun.common.enums.MessageTypeEnums;
import org.springframework.stereotype.Component;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 事件处理器注解
 * @Author: https://gitee.com/wesleyOne
 * @Date: 12.08 2019
 */
@Component
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface SxEventProcessor {

    /**
     * 处理器关联类型
     */
    MessageTypeEnums messageType();
}
