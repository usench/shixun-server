package com.easy.lab.shixun.common.utils;

import org.apache.commons.lang3.RandomStringUtils;

/**
 * @Author: https://gitee.com/wesleyOne
 * @Date: 12.01 2019
 */
public class RandomStringUtil {

    public static String createToken() {
        return RandomStringUtils.randomAlphanumeric(12);
    }

    public static String createGroupUniqueId() {
        return RandomStringUtils.randomAlphanumeric(6);
    }
}
